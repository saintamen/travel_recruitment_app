package biz.solution;

import biz.solution.adapters.models.IMyFlightConnectionModel;
import biz.solution.adapters.services.IServiceAdapter;
import biz.solution.adapters.services.LowCostFlightSupplierWsAdapter;
import biz.solution.adapters.services.RegularFlightSupplierWsAdapter;
import biz.supplier.LowcostFlightSupplierWS;
import biz.supplier.RegularFlightSupplierWS;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<IServiceAdapter<?>> serviceAdapters = new ArrayList<>();
        serviceAdapters.add(new LowCostFlightSupplierWsAdapter(new LowcostFlightSupplierWS()));
        // od teraz obsługujemy nowego dostawcę połączeń lotniczych
        serviceAdapters.add(new RegularFlightSupplierWsAdapter(new RegularFlightSupplierWS()));

        List<IMyFlightConnectionModel> flights = serviceAdapters.stream()
                .flatMap(supplier -> supplier.getFlightConnections("bar", "gda", new Date(System.currentTimeMillis()), 10).stream())
                .collect(Collectors.toList());

        for (IMyFlightConnectionModel flight : flights) {
            System.out.println(flight);
        }

    }
}
