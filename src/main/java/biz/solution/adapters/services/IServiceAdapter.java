package biz.solution.adapters.services;

import biz.solution.adapters.models.IMyFlightConnectionModel;

import java.util.Date;
import java.util.List;

public interface IServiceAdapter<T> {
    //list(String destCode, String deptCode, Date date, Integer maxResults) throws IOException
    //getConnections(String destCode, String deptCode, Date date, Integer maxResults) throws IOException
    List<IMyFlightConnectionModel> getFlightConnections(String destCode, String deptCode, Date date, Integer maxResults);

    List<IMyFlightConnectionModel> mapConnectionFlights(List<T> cheapFlights);
}
