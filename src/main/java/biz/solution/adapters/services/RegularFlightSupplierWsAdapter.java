package biz.solution.adapters.services;

import biz.solution.adapters.models.IMyFlightConnectionModel;
import biz.solution.adapters.models.MyFlightConnectionModel;
import biz.solution.adapters.MyMoney;
import biz.supplier.RegularFlightSupplierWS;

import java.io.IOException;
import java.time.Duration;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class RegularFlightSupplierWsAdapter implements IServiceAdapter<RegularFlightSupplierWS.Connection> {
    private final RegularFlightSupplierWS regularFlightSupplierWS;

    public RegularFlightSupplierWsAdapter(RegularFlightSupplierWS regularFlightSupplierWS) {
        this.regularFlightSupplierWS = regularFlightSupplierWS;
    }

    @Override
    public List<IMyFlightConnectionModel> getFlightConnections(String destCode, String deptCode, Date date, Integer maxResults) {
        List<IMyFlightConnectionModel> listOfResults = new ArrayList<>();
        try {
            List<RegularFlightSupplierWS.Connection> cheapFlights = regularFlightSupplierWS.getConnections(destCode, deptCode, date, maxResults);

            listOfResults.addAll(mapConnectionFlights(cheapFlights));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfResults;
    }

    public List<IMyFlightConnectionModel> mapConnectionFlights(List<RegularFlightSupplierWS.Connection> cheapFlights) {
        return cheapFlights
                .stream()
                .map(regularFlight -> MyFlightConnectionModel.builder()
                        .id(String.valueOf(regularFlight.getId()))
                        .departureCode(regularFlight.getDepartureCode())
                        .destinationCode(regularFlight.getDestinationCode())
                        .departureTime(regularFlight.getDateTime().toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                        .numberOfTransfers(null)
                        .price(new MyMoney(regularFlight.getPrice().getAmount(), regularFlight.getPrice().getCurrency()))
                        .travelDuration(Duration.ofSeconds(regularFlight.getDuration().getStandardSeconds()))
                        .build())
                .collect(Collectors.toList());
    }
}
