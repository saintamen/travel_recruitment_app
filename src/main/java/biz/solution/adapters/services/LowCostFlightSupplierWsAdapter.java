package biz.solution.adapters.services;

import biz.solution.adapters.models.IMyFlightConnectionModel;
import biz.solution.adapters.models.MyFlightConnectionModel;
import biz.solution.adapters.MyMoney;
import biz.supplier.LowcostFlightSupplierWS;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class LowCostFlightSupplierWsAdapter implements IServiceAdapter<LowcostFlightSupplierWS.CheapFlight> {
    private final LowcostFlightSupplierWS lowCostFlightSupplierWs;

    public LowCostFlightSupplierWsAdapter(LowcostFlightSupplierWS lowCostFlightSupplierWs) {
        this.lowCostFlightSupplierWs = lowCostFlightSupplierWs;
    }

    @Override
    public List<IMyFlightConnectionModel> getFlightConnections(String destCode, String deptCode, Date date, Integer maxResults) {
        List<IMyFlightConnectionModel> listOfResults = new ArrayList<>();
        try {
            List<LowcostFlightSupplierWS.CheapFlight> cheapFlights = lowCostFlightSupplierWs.list(destCode, deptCode, date, maxResults);

            listOfResults.addAll(mapConnectionFlights(cheapFlights));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfResults;
    }

    public List<IMyFlightConnectionModel> mapConnectionFlights(List<LowcostFlightSupplierWS.CheapFlight> cheapFlights) {
        return cheapFlights
                .stream()
                .map(cheapFlight -> MyFlightConnectionModel.builder()
                        .id(String.valueOf(cheapFlight.getId()))
                        .departureCode(cheapFlight.getDeptCode())
                        .destinationCode(cheapFlight.getDestCode())
                        .departureTime(LocalDateTime.from(cheapFlight.getTime().toInstant().atZone(ZoneId.systemDefault())))
                        .numberOfTransfers(cheapFlight.getNumberOfTransfers())
                        .price(new MyMoney(new BigDecimal(cheapFlight.getPrice()), Currency.getInstance(cheapFlight.getCurrency())))
                        .travelDuration(null)
                        .build())
                .collect(Collectors.toList());
    }
}
