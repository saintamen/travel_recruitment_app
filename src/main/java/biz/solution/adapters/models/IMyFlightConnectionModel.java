package biz.solution.adapters.models;

import biz.solution.adapters.MyMoney;

import java.time.Duration;
import java.time.LocalDateTime;

public interface IMyFlightConnectionModel {
    //Flight{, , , , , }
    //Connection{, , , , , }

                    //id=0
                    //id='afd9f794-7c21-4ae2-a24b-34119d03a98e'
    String getId(); // jeśli liczba to zamień na String

                                    // destinationCode='bar'
                                    // destCode='bar'
    String getDestinationCode();    //

                                    // departureCode='gda'
                                    // deptCode='gda'
    String getDepartureCode();      //

                                        //  time=Tue Aug 10 21:47:00 CEST 2021
                                        //  dateTime=2021-08-10T15:45:00.000
    LocalDateTime getDepartureTime();   //

                                        //  numberOfTransfers=2
                                        //  null
    Integer getNumberOfTransfers();     //

                                        //  amount=Money{amount=444, currency=EUR}
                                        //  price='309.97', currency='EUR'
    MyMoney getPrice();                 //

                                        // null
                                        // duration=PT3600S
    Duration getTravelDuration();       //
}
