package biz.solution.adapters.models;

import biz.solution.adapters.MyMoney;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MyFlightConnectionModel implements IMyFlightConnectionModel{
    private String id;
    private String destinationCode;
    private String departureCode;
    private LocalDateTime departureTime;
    private Integer numberOfTransfers;
    private MyMoney price;
    private Duration travelDuration;
}
