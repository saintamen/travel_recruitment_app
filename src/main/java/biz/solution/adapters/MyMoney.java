package biz.solution.adapters;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Currency;

@Data
@AllArgsConstructor
public class MyMoney {
    private final BigDecimal amount;
    private final Currency currency;
}
